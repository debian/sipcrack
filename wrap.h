/* Copyright (C) 2007  Martin J. Muench <mjm@codito.de> */

#ifndef SIPCRACK_WRAPPER_H
#define SIPCRACK_WRAPPER_H

#include <stdio.h>
#include "global.h"

void *Malloc(size_t);
void *Realloc(void *, size_t);
char **stringtoarray(char *, char, int *);
void get_string_input(char *, size_t, const char *, ...);
int  is_binary(const unsigned char *, size_t);
void init_bin2hex(char [256][2]);
void bin_to_hex(char [256][2], const unsigned char *, size_t,  char *, size_t);
void write_login_data(login_t *, const char *);
void update_login_data(login_t *, const char *, const char *);
int  find_value(const char *, const char *, char *, size_t);
void Toupper(char *, size_t);
void extract_method(char *, const char *, size_t);

#endif /* SIPCRACK_WRAPPER_H */


