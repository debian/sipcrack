/* Copyright (C) 2007  Martin J. Muench <mjm@codito.de> */

#ifndef	SIPCRACK_DEBUG_H
#define SIPCRACK_DEBUG_H

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#ifdef DEBUG
#define debug(x) ic_debug x;
#else 
#define debug(x) do { } while(1!=1);
#endif

void ic_debug(const char *fmt, ...);

#endif /* SIPCRACK_DEBUG_H */
